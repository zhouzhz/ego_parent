package com.hr.service;


import com.hr.commons.pojo.EgoResult;

public interface TbItemDescService {
    EgoResult selectById(Long id);
}
