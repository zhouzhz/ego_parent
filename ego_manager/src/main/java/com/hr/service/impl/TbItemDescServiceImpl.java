package com.hr.service.impl;

import com.hr.commons.pojo.EgoResult;
import com.hr.dubbo.service.TbItemDescDubboService;
import com.hr.pojo.TbItemDesc;
import com.hr.service.TbItemDescService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.stereotype.Service;

@Service
public class TbItemDescServiceImpl implements TbItemDescService {
    @Reference
    private TbItemDescDubboService tbItemDescDubboService;
    @Override
    public EgoResult selectById(Long id) {
        TbItemDesc tbItemDesc = tbItemDescDubboService.selectById(id);
        return EgoResult.ok(tbItemDesc);
    }
}
