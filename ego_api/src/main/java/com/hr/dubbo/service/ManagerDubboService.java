package com.hr.dubbo.service;


import com.hr.pojo.Manager;

/**
 * 对manager表操作
 */
public interface ManagerDubboService {
    /**
     * 根据用户名查询后台用户信息
     * @param username 用户名
     * @return 用户详情
     */
    Manager selectManagerByUsername(String username);
}
