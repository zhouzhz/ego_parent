package com.hr.dubbo.service;


import com.hr.pojo.TbItemDesc;

public interface TbItemDescDubboService {
    /**
     * 根据主键查询对象信息
     * @param id
     * @return
     */
    TbItemDesc selectById(Long id);
}
