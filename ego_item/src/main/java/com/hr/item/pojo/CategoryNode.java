package com.hr.item.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

// 返回的菜单项
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryNode {

    private String u;
    private String n;
    private List<Object> i;
}
