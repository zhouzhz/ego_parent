package com.hr.item.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

// 总体数据
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemCategoryNav {
    private List<Object> data;
}
