package com.hr.dubbo.service.impl;

import com.hr.dubbo.service.TbItemDescDubboService;
import com.hr.mapper.TbItemDescMapper;
import com.hr.pojo.TbItemDesc;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class TbItemDescDubboServiceImpl implements TbItemDescDubboService {
    @Autowired
    private TbItemDescMapper tbItemDescMapper;
    @Override
    public TbItemDesc selectById(Long id) {
        return tbItemDescMapper.selectByPrimaryKey(id);
    }
}
